#!/bin/bash

### SCOTCH ####
echo "Download SCOTCH"
# wget -c "https://gforge.inria.fr/frs/download.php/file/34618/scotch_6.0.4.tar.gz"
# git clone -b starpart https://gitlab.inria.fr/metapart/scotch.git scotch
# git archive --prefix=scotch/ --format=zip --remote=git@gitlab.inria.fr:metapart/scotch6-unofficial.git starpart > scotch.zip
# git clone https://gitlab.inria.fr/scotch/scotch.git scotch
# curl -O "https://gitlab.inria.fr/scotch/scotch/-/archive/scotch_6.0/scotch-scotch_6.0.tar.gz"
git archive --prefix=scotch/ --format=tgz --remote="git@gitlab.inria.fr:scotch/scotch.git" scotch_6.0 > scotch-6.0.tgz

### METIS ####
echo "Download METIS"
wget -c "http://glaros.dtc.umn.edu/gkhome/fetch/sw/metis/metis-5.1.0.tar.gz" -O metis-5.1.0.tgz

### PARMETIS ####
echo "Download PARMETIS"
wget -c "http://glaros.dtc.umn.edu/gkhome/fetch/sw/parmetis/parmetis-4.0.3.tar.gz"-O /parmetis-4.0.3.tgz

### MTMETIS ####
echo "Download MTMETIS"
wget -c "http://glaros.dtc.umn.edu/gkhome/fetch/sw/metis/mt-metis-0.6.0.tar.gz"

### ZOLTAN ###
echo "Download ZOLTAN"
wget -c "http://www.cs.sandia.gov/~kddevin/Zoltan_Distributions/zoltan_distrib_v3.83.tar.gz" -O zoltan-3.83.tgz

### PATOH ###
echo "Download PATOH"
wget -c "https://www.cc.gatech.edu/~umit/PaToH/patoh-Linux-x86_64.tar.gz" -O patoh-3.2.tgz

### KAHIP ###
echo "Download KAHIP"
wget -c "http://algo2.iti.kit.edu/schulz/software_releases/KaHIP_2.00.tar.gz" -O kahip-2.0.tgz

### MONDRIAAN ###
echo "Download MONDRIAAN"
wget -c "http://www.staff.science.uu.nl/~bisse101/Mondriaan/mondriaan_v4.2.tar.gz" -O mondriaan-4.2.tgz

### PULP ###
echo "Download PULP"
# git clone "https://github.com/HPCGraphAnalysis/PuLP.git" pulp
wget -c "https://github.com/HPCGraphAnalysis/PuLP/archive/master.tar.gz" -O pulp-0.2.tgz
# git archive --prefix=pulp/ --format=zip --remote="https://github.com/HPCGraphAnalysis/PuLP.git"  master > pulp.zip # not supported by github
# svn export https://github.com/HPCGraphAnalysis/PuLP/trunk pulp

echo "Done!"

